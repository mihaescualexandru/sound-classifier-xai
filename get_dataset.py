import os
import tarfile

import wget


def get_dataset():
    url = 'https://zenodo.org/record/1203745/files/UrbanSound8K.tar.gz?download=1'
    filename = wget.download(url)

    try:
        file = tarfile.open(filename,'r:gz')
        try: 
            file.extractall()
        finally: 
            file.close()
    except Exception as e:
        print(e)

if __name__ == "__main__":
    get_dataset()

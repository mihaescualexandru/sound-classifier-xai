import os

print('I hope this has admin right cuz it ain\'t gonna run without')
import librosa
import numpy as np
import pandas as pd
from tqdm import tqdm


def get_classes(df):
    classes = list(df['class'].unique())
    pairs = [(c, df.loc[df['class'] == c].iloc[0]['classID']) for c in classes]
    pairs.sort(key=lambda x:x[1])
    return [c[0] for c in pairs]

def norm(arr):
    arr = arr.astype(np.float32)
    m = np.max(arr)
    if m == 0:
        return np.zeros_like(arr)
    arr *= 1.0/m
    return arr

def get_stuff(y, sr):
    y,sr=librosa.load(base + fold + '/' + file)
    mfccs = norm(librosa.feature.mfcc(y, sr, n_mfcc=v_size))
    melspectrogram = norm(librosa.feature.melspectrogram(y=y, sr=sr, n_mels=v_size,fmax=8000))
    chroma_stft= norm(librosa.feature.chroma_stft(y=y, sr=sr,n_chroma=v_size))
    chroma_cq = norm(librosa.feature.chroma_cqt(y=y, sr=sr,n_chroma=v_size))
    chroma_cens =norm(librosa.feature.chroma_cens(y=y, sr=sr,n_chroma=v_size))
    return [mfccs, melspectrogram, chroma_stft, chroma_cq, chroma_cens]

def do_stuff(stuff):
    v_size, h_size = 50, 10

    if not len(np.unique(np.array([x.shape for x in stuff]))) == 2:
        print('Inconsistency in results')
        return None
    
    data = []
    w = stuff[0].shape[1]
    
    for i in range(0, w-h_size, h_size):
        layer = []
        for x in stuff:
            layer.append(x[:,i:i+h_size])
        data.append(np.hstack(layer))

    if not w % h_size == 0:
        surp = np.zeros((v_size, h_size*5), np.float32)
        for i in range(5):
            surp[:,h_size*i:h_size*i+w% h_size] = stuff[i][:, w-w%h_size:]
        data.append(surp)

    if len(data) > 1: return np.stack(data, axis=0)    
    elif len(data) == 1: return data[0].reshape(1, v_size, -1)
    else: return None

def save_stuff(done_stuff, fold, file):
    if not done_stuff is None: np.save(f'./dataset/{fold}/{file}.npy', done_stuff)

def prep_data():
    base = 'UrbanSound8K/audio/'

    for fold in os.listdir(base):
        if fold == '.DS_Store': continue
        print(f'Doing {fold}')
        cnt, size = 0, len(os.listdir(base+fold))
        if not os.path.isdir(f'./dataset/{fold}'):
            os.makedirs(f'./dataset/{fold}')
        
        for file in os.listdir(base+fold):
            if file == '.DS_Store': continue
            y,sr=librosa.load(base + fold + '/' + file)
            stuff = get_stuff(y, sr)
            done_stuff = do_stuff(stuff)
            save_stuff(done_stuff, fold, file)
            print(f'Done {cnt} out of {size-2}', end='\r')
            cnt += 1
        print()

if __name__ == "__main__":
    prep_data()

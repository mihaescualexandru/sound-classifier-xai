import os
import pickle
import sys

import numpy as np
import pandas as pd
import torch
import torch.nn as nn
import torch.optim as optim
import torchvision
import torchvision.transforms as transforms
from tqdm import tqdm

from model import Net
from prepare_dataset import get_classes, norm


def get_data(filename):
    try:
        if os.path.isfile(filename):
            return [norm(x) for x in np.load(filename)]
        else:
            return []
    except:
        return []

def get_train_test():

    if os.path.isfile('./data.bin'):
        with open('./data.bin', 'rb') as f:
            data = pickle.load(f)
            return data

    data=pd.read_csv("./UrbanSound8K/metadata/UrbanSound8K.csv")

    x_train=[]
    x_test=[]
    y_train=[]
    y_test=[]

    path="./dataset/fold"

    for i in tqdm(range(len(data))):
        fold_no=str(data.iloc[i]["fold"])
        file=data.iloc[i]["slice_file_name"]
        label=data.iloc[i]["classID"]
        filename=path+fold_no+"/"+file

        stuff = get_data(filename+'.npy')
        
        if(fold_no!='10'):
            x_train += stuff
            y_train += [label]*len(stuff)
        else:
            x_test += stuff
            y_test += [label]*len(stuff)

    x_train = np.array(x_train)
    x_test  = np.array(x_test)
    y_train = np.array(y_train)
    y_test  = np.array(y_test)

    with open('./data.bin', 'wb') as f:
            pickle.dump((x_train, x_test, y_train, y_test), f)

    return x_train, x_test, y_train, y_test


def train(epochs):
    x_train, x_test, y_train, y_test = get_train_test()  
    #shape
    print(x_train.shape,x_test.shape,y_train.shape,y_test.shape)

    trainloader = torch.utils.data.DataLoader(list(zip(x_train, y_train)), batch_size=128,
                                            shuffle=True, num_workers=0)
    testloader  = torch.utils.data.DataLoader(list(zip(x_test, y_test)), batch_size=128,
                                          shuffle=False, num_workers=0)

    net = Net(trainloader, testloader).cuda()

    if os.path.isfile('./net.pt'):
        net.load_state_dict(torch.load('./net.pt'))
    
    criterion = nn.CrossEntropyLoss()
    optimizer = optim.Adam(net.parameters(), lr=0.001)
    net.train_(epochs, optimizer, criterion)

    torch.save(net.state_dict(), './net.pt')

    return net 

if __name__ == "__main__":
    if len(sys.argv) == 2:
        train(int(sys.argv[1]))
    else:
        print('Please specify the number of epochs when running the script')
